# MercedesBenzTechnicalInterview

## The Scenario

One of your software engineering colleagues at Mercedes-Benz Mobility comes to you and needs your support deploying a dockerized web application on our multi-tenant Kubernetes Cluster which is reachable outside the cluster.

The application must be deployed in a namespace which already contains other applications.

It is expected that the application will face traffic spikes on weekends.

Please provide resources that can be used to deploy the application securely on multiple environments.

The colleague shared the following lines on how to run the dockerized application locally:

Please use `docker load` to load the shared tarball. 

The application needs two environment variables to run, the license key `license`,

which is confidential and should be set to `53cr3t` and the `name`, which should be `Carl Benz`.

The web application is exposed on port `8080`.

 

```cli
docker run -e name="Carl Benz" -e license=53cr3t -p 8080:8080 app
curl "http://localhost:8080/api/v1/hello"
curl "http://localhost:8080/actuator/metrics"
```

### Notes:

For simplicity reasons, the application can run locally and be reachable on `http://localhost:8080/api/v1/hello`.
Tools that might be used to solve the exercise are Helm, Kustomize or Terraform.
Watch out the app is somehow not the fastest on starting up it takes about 30 sec.

If you can share your code already before the meeting on Tuesday that would be great so we can also prepare questions based on that.

You can share the resources as a zip file.


## The plan:

create cluster
install terraform
create workspaces dev

run application as single deployment with 1 replica

customize deployment for stage and prod
set up permissions 

load test
test health
display metrics
set up vertical pod autoscaling

create terraform module

# The solution

Create a Terraform module which allows different settings for each environment and can be flexible enough to be used by various engineers. 
The web deployment module allows its users to deploy any image by specifying a namespace, the environment and the secret key. It creates said namespace and deploys the image on creating a sersvices that publishes the URL on the host.
A developer role is also created which allows users to create and delete further resources inside that namespace.
If the deployment is set for production it restricts any account binded to the developer role from modification.