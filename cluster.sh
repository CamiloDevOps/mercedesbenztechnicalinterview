#!/bin/bash


if [ $1 == "setup" ]; then

    # Add Terraform repository mirror
    wget -O- https://apt.releases.hashicorp.com/gpg | gpg --dearmor | sudo tee /usr/share/keyrings/hashicorp-archive-keyring.gpg
    echo "deb [signed-by=/usr/share/keyrings/hashicorp-archive-keyring.gpg] https://apt.releases.hashicorp.com $(lsb_release -cs) main" | sudo tee /etc/apt/sources.list.d/hashicorp.list
    sudo apt-get update && sudo apt-get install terraform=1.3.7
    
    # Install Go for Terratest
    wget https://dl.google.com/go/go1.13.5.linux-amd64.tar.gz
    sudo tar -C /usr/local/ -xzf go1.13.5.linux-amd64.tar.gz
    export PATH=/usr/local/go/bin:$PATH

    # Update Ubuntus package lists

    # Install Docker
    echo "Installing Docker";
    #sudo apt-get install -y docker.io

    # Install minikube
    echo "Installing Minikube";
    curl -Lo minikube https://storage.googleapis.com/minikube/releases/latest/minikube-linux-amd64
    chmod +x minikube
    mv minikube /usr/local/bin/
    minikube version
   
    # Install kubectl
    echo "Installing Kubectl";
    curl -LO "https://dl.k8s.io/release/`curl -L https://dl.k8s.io/release/stable.txt`/bin/linux/amd64/kubectl"
    chmod +x kubectl
    sudo mv kubectl /usr/local/bin/

    # Start minikube
    # The settings for cpus and memory may need to be changed depending on the machine running the minikube cluster     
    echo "Creating cluster";
    minikube start -p mercedes-cluster --cpus 3 --memory 5072
    minikube addons enable metrics-server
    eval $(minikube docker-env -p mercedes-cluster)
    docker load -i javaapp.tar
    minikube status
   
    echo "Starting Kubernetes dashboard"
    minikube dashboard &

    # Install helm
    # echo "Installing Helm 3";
    # curl -fsSL -o get_helm.sh https://raw.githubusercontent.com/helm/helm/main/scripts/get-helm-3
    # chmod 700 get_helm.sh
    # ./get_helm.sh


    # Create Terraform workspace
    cd terraform
    mkdir terraform/stage
    terraform init
    terraform workspace new stage
    
fi

if [ $1 == "uninstall" ]; then
    echo "Uninstalling minikube";
    minikube stop && minikube delete --purge=true -p mercedes-cluster
fi

