package test

var terraformDir string = "stage"
var tfVarsFile string = "stage.tfvars"
var clusterNameStage string = "mercedes-cluster"
var namespaceStage string = "mobility-stage"

var stageVars = map[string]interface{}{
	"tfVarsFile":   tfVarsFile,
	"terraformDir": terraformDir,
	"clusterName":  clusterNameStage,
	"namespace":    namespaceStage,
}
