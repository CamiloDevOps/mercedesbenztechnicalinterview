package test

import (
	"fmt"
	"net/http"
	"os"
	"testing"

	"github.com/gruntwork-io/terratest/modules/k8s"
	"github.com/gruntwork-io/terratest/modules/terraform"
	"github.com/stretchr/testify/assert"
)

//var kubectlOptions = k8s.NewKubectlOptions("mercedes-cluster", "", namespace)
var kubectlOptions k8s.KubectlOptions
var environment string = os.Getenv("WEB_APP_ENV")
var Fixtures map[string]interface{}
var tfDir string
var clusterName string
var namespace string

func TestSetUp(t *testing.T) {
	if environment == "stage" {
		fmt.Println("******** Settting up test for Stage env ********")
		fmt.Println("stageVars: ")
		fmt.Println(stageVars)

		Fixtures = stageVars
		tfDir = fmt.Sprintf("../%v", Fixtures["terraformDir"])
		clusterName = fmt.Sprintf("%v", Fixtures["clusterName"])
		namespace = fmt.Sprintf("%v", Fixtures["namespace"])
	}
}

func TestFailIfNamespaceDoesNotExists(t *testing.T) {

	// t.Parallel()
	terraformOptions := terraform.WithDefaultRetryableErrors(t, &terraform.Options{
		TerraformDir: tfDir,
	})

	outputs := terraform.OutputAll(t, terraformOptions)

	assert.NotNil(t, outputs["namespace"])
}

func TestFailsIfDeploymentDoesNotExist(t *testing.T) {

	// t.Parallel()
	terraformOptions := terraform.WithDefaultRetryableErrors(t, &terraform.Options{
		TerraformDir: tfDir,
	})

	outputs := terraform.OutputAll(t, terraformOptions)
	assert.NotNil(t, outputs["deployment"], "Deployment should not be nil")

}

func TestFailsIfApplicationIsNotAccessible(t *testing.T) {

	// t.Parallel()
	terraformOptions := terraform.WithDefaultRetryableErrors(t, &terraform.Options{
		TerraformDir: tfDir,
	})

	outputs := terraform.OutputAll(t, terraformOptions)
	assert.NotNil(t, outputs["service"], "Service should not be nil")

	value, _ := outputs["service-local-url"].(string)
	resp, err := http.Get(value)

	assert.Nil(t, err, "Error accessing the application")
	assert.Equal(t, http.StatusOK, resp.StatusCode, "Unexpected response code")
}

func TestFailsIfDeveloperRoleDoesNotExist(t *testing.T) {
	// t.Parallel()

	terraformOptions := terraform.WithDefaultRetryableErrors(t, &terraform.Options{
		TerraformDir: tfDir,
	})

	outputs := terraform.OutputAll(t, terraformOptions)

	assert.NotNil(t, outputs["developer-role"], "Role should not be nil")

}

func TestFailsIfDeveloperRoleBindingDoesNotExists(t *testing.T) {
	//t.Parallel()
	terraformOptions := terraform.WithDefaultRetryableErrors(t, &terraform.Options{
		TerraformDir: tfDir,
	})

	outputs := terraform.OutputAll(t, terraformOptions)

	assert.NotNil(t, outputs["developer-role-binding"], "Role binding should not be nil")
}

/*
Create a test service account and associate it with the developer role binding to check if it can create
resources in other namespaces

*/
func TestDeveloperRoleCannotCreateDeploymentOnDefaultNamespace(t *testing.T) {

	kubectlOptions := k8s.NewKubectlOptions(clusterName, "", namespace)

	deploymentName := "test-deployment.yml"
	// serviceAccountName := "test-serviceaccount"
	roleName := "developer"
	roleBindingName := "test-role-binding"

	role, roleError := k8s.GetRoleE(t, kubectlOptions, roleName)
	assert.NotNil(t, role, "Role does not exists")
	assert.Nil(t, roleError, "Error getting role")

	terraformOptions := terraform.WithDefaultRetryableErrors(t, &terraform.Options{
		TerraformDir: tfDir,
	})

	outputs := terraform.OutputAll(t, terraformOptions)
	serviceAccount := outputs["ops-service-account"]

	// serviceAccountError := k8s.CreateServiceAccountE(t, kubectlOptions, serviceAccountName)
	// assert.Nil(t, serviceAccountError, "Error creating test service account")

	// testServiceAccount, serviceAccountError := k8s.GetServiceAccountE(t, kubectlOptions, serviceAccountName)
	// assert.NotNil(t, testServiceAccount, "Service account not found")

	fmt.Println("testServiceAccount: ")
	fmt.Println(serviceAccount)

	// Create role binding
	//kubectl create rolebinding developer-binding --role=developer --serviceaccount=default:test-serviceaccount

	// roleBindingError := k8s.RunKubectlE(t, kubectlOptions, "create", "rolebinding", roleBindingName,
	// 	"--role="+roleName, "--serviceaccount="+namespace+":"+serviceAccountName, "--namespace="+namespace)
	// assert.Nil(t, roleBindingError, "Error binding role to test service account")

	// fmt.Println("roleBindingError:")
	// fmt.Println(roleBindingError)

	// token, tokenError := k8s.GetServiceAccountAuthTokenE(t, kubectlOptions, serviceAccountName)
	// assert.Nil(t, tokenError, "Error getting service account auth token")

	// addConfigError := k8s.AddConfigContextForServiceAccountE(t, kubectlOptions, clusterName, serviceAccountName, token)
	// assert.Nil(t, addConfigError, "Error addding config for test service account")

	// updateConfigError := k8s.RunKubectlE(t, kubectlOptions, "config", "set-context", clusterName, "-n", namespace)
	// assert.Nil(t, updateConfigError, "Error updating context for test services account")

	// secretError := k8s.RunKubectlE(t, kubectlOptions, "apply", "-f", "test-secret-token.yml", "-n", namespace)
	// assert.Nil(t, secretError, "Error creating test secret")

	//kubectl --as=system:serviceaccount:<namespace>:<service_account_name> run <deployment_name> --image=<image>
	// createDeploymentError := k8s.RunKubectlE(t, kubectlOptions, "apply", "-f", deploymentName, "-n", "default")
	// assert.Error(t, createDeploymentError)

	// // Clean up the deployment
	// deleteDeploymentError := k8s.RunKubectlE(t, kubectlOptions, "delete", "deployment", "-n", namespace, deploymentName)
	// assert.NoError(t, deleteDeploymentError, "Error deleting test deployment")

	// deleteServiceAccountError := k8s.RunKubectlE(t, kubectlOptions, "delete", "serviceaccount", serviceAccountName)
	// assert.Nil(t, deleteServiceAccountError, "Error deleting test service account")

	// deleteDeleteRoleBindingError := k8s.RunKubectlE(t, kubectlOptions, "delete", "rolebinding", roleBindingName, "-n", namespace)
	// assert.Nil(t, deleteDeleteRoleBindingError, "Error deleting test role binding")

}
