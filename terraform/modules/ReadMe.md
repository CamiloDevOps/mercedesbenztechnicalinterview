Terraform Web app deployment Module
===========

A terraform module to provision a Docker image in a given namespace and cluster


Module Input Variables
----------------------

- `cluster_name` - string - Creates the name of the cluster resource together with the workspace name

- `namespace` - boolean - Enable private endpoint access for internal cluster communication

- `image` -  string -  name and tag of the image. Ex: "app:latest"

- `env` - string - The name of the environment where to deploy the app.


Usage
-----

```hcl
module "deploy" {
  source  = "./web-deployment"
  namespace  = var.namespace
  image   = var.image
}
```


Outputs
=======

 - `public_subnet_1_id` - Id of the created subnet
 

 

Authors
=======

fernandez.camilo@live.com