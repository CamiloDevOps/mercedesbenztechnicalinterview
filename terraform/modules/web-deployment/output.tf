output "namespace" {
  value       = kubernetes_namespace.mobility
  description = "Namespace"
}

output "deployment" {
  value       = kubernetes_deployment.web-app
  description = "The deployment of the web app"
}

output "service" {
  value       = kubernetes_service.web_app
  description = "Service of type ClusterIp managing the networking entrypoint for the web application"
}

output "service-local-url" {
  
  value       = "http://localhost:${kubernetes_service.web_app.spec[0].port[0].port}/api/v1/hello"
  description = "Url of the application"
}

output "developer-role" {
  value       = kubernetes_role.developer_role
  description = "Kubernetes role for developers"
}

output "developer-role-binding" {
  value       = kubernetes_role_binding.developer_binding
  description = "Role binding for developer role"
}

output "ops-service-account" {
  value       = kubernetes_service_account.service-account.metadata.name
  description = "Service account for operation team"
}

