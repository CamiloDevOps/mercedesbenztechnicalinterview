resource "kubernetes_namespace" "mobility" {
  metadata {
    name = var.namespace
  }
}


locals {
  app-name = var.app-name
  role-name = var.role-name
  service-account = var.service-account
}



resource "kubernetes_secret" "app-key" {
  metadata {
    name      = "app-key"
    namespace = var.namespace
  }

  data = {
    license = "${base64encode(var.app-key)}"
  }
}

resource "kubernetes_deployment" "web-app" {
  metadata {
    name      = local.app-name
    namespace = var.namespace
  }

  spec {
    replicas = 1

    selector {
      match_labels = {
        app = local.app-name
      }
    }

    template {
      metadata {
        labels = {
          app = local.app-name
        }
      }

      spec {
        container {
          name  = local.app-name
          image = var.image
          image_pull_policy = "Never"

          env {
            name = "license"
            value_from {
              secret_key_ref {

                # add to vars
                name = "${kubernetes_secret.app-key.metadata.0.name}"
                key  = "license"
              }
            }
          }

          env {
            name  = "name"
            value = var.name
          }

          port {
            name          = "http"
            container_port = 8080
          }
        }
      }
    }
  }
}



resource "kubernetes_service" "web_app" {
  metadata {
    name = local.app-name
    namespace = var.namespace
  }

  spec {
    type        = "ClusterIP"
    selector    = {
      app = local.app-name
    }
    port {
      name = "http"
      port = 3000
      target_port = 8080
    }
  }
}

# Allow creation and deletion of resources within namespace while denying deletion on all others namespaces.
resource "kubernetes_role" "developer_role" {
  metadata {
    name      = local.role-name
    namespace = var.namespace
  }

  rule {
    api_groups = ["apps"]
    resources  = ["deployments", "daemonsets", "replicasets", "statefulsets"]
    verbs      = ["create", "list", "update", "watch", "delete"]
  }

  rule {
    api_groups = [""]
    resources = ["pods", "services", "secrets", "endpoints", "serviceaccounts", "configmaps"]
    verbs      = ["create", "list", "update", "watch", "delete"]
  }

}


resource "kubernetes_role_binding" "developer_binding" {
  metadata {
    name      = local.role-name
    namespace = var.namespace
  }

  role_ref {
    api_group = "rbac.authorization.k8s.io"
    kind      = "Role"
    name      = "${kubernetes_role.developer_role.metadata[0].name}"
  }

  subject {
    api_group = "rbac.authorization.k8s.io"
    kind      = "User"
    name      = local.role-name
  }
  
  subject {
    api_group = "rbac.authorization.k8s.io"
    kind      = "serviceaccounts"
    name      = local.service-account
  }
}

resource "kubernetes_service_account" "service-account" {
  metadata {
    name      = var.service-account
    namespace = var.namespace
  }
}
