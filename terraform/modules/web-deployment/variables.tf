variable "app-key" {
  type        = string
  description = "The secret"
  sensitive   = true
}

variable "app-name" {
  default     = "web-app"
  type        = string
  description = "Name for the deployment"
}

variable "role-name" {
  default     = "developer"
  type        = string
  description = "Name for the role and service account"
}

variable "name" {
  default     = "Carl Benz"
  type        = string
  description = "The app name"
}

variable "image" {
  type        = string
  description = "Docker image with tag. Ex: hello:test"
}

variable "namespace" {
  default     = "mobility"
  type        = string
  description = "Kubernetes namespace where the web app is to be deployed"
}

variable "service-account" {
  default     = "ops-service-account"
  type        = string
  description = "Services account used by the operations team for managing cluster resources"
}
