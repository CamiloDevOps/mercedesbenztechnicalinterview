

output "namespace" {
  value       = module.web-app-deployment.namespace
  description = "Namespace where the application is being deployed"
}

output "deployment" {
  value       = module.web-app-deployment.deployment
  description = "The deployment of the web app"
}

output "service" {
  value       = module.web-app-deployment.service
  description = "Service of type ClusterIp managing the networking entrypoint for the web application"
}

output "service-local-url" {
  
  value       = module.web-app-deployment.service-local-url
  description = "Url of the application"
}

output "developer-role" {
  value       = module.web-app-deployment.developer-role
  description = "Kubernetes role for developers"
}

output "developer-role-binding" {
  value       = module.web-app-deployment.developer-role-binding
  description = "Role binding for developer role"
}
