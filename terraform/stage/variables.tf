variable "app-key" {
  type        = string
  description = "The secret"
  sensitive   = true
}

variable "name" {
  type        = string
  description = "The app name"
}

variable "namespace" {
  default     = "mobility"
  type        = string
  description = "Kubernetes namespace where the web app is to be deployed"
}

variable "image" {
  type        = string
  description = "Docker image with tag. Ex: hello:test"
}

variable "environment" {
  type        = string
  description = "Environment where to deploy the app. Ex: stage, prod"
}

variable "app-name" {
  type        = string
  description = ""
}

variable "role-name" {
  type        = string
  description = "Name for the role and service account"
}

variable "service-account" {
  type        = string
  description = "Name for the service account to be used by operations"
}


