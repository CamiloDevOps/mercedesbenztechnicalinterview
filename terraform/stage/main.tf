

module "web-app-deployment" {
  source = "../modules/web-deployment"
  namespace = var.namespace
  app-key = var.app-key
  image = var.image
  role-name = var.role-name
  service-account = var.service-account
  
}
