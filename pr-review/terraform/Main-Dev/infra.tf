# Inconsistent case and variable naming
# Defaults missing
# Description missing
# Inconsistent line break

variable "ARM_CLIENT_ID" {
  description = "Cient ID for service principal"
  type        = string
}
variable "ARM_CLIENT_SECRET" {
  description = "Client secret for service principal"
  type        = string
  sensitive   = true
}
variable "ARM_SUBSCRIPTION_ID" {
  description = "Subscription for enviroment"
  type        = string
}
variable "ARM_TENANT_ID" {
  description = "Tenant ID for enviroment"
  type        = string
}

variable "licenseKey" {
  type = string
}

# Variables not provided, prompt will appear
variable "location" {
}

variable "main_address_space" {
}

variable "main_address_prefixes" {
}

# Linter not applied. Indentation is inconsistent
provider "azurerm" {
  features {
    key_vault {
      purge_soft_deleted_secrets_on_destroy = true
      recover_soft_deleted_secrets          = false
    }
  }

subscription_id = var.ARM_SUBSCRIPTION_ID
client_id = var.ARM_CLIENT_ID
client_secret = var.ARM_CLIENT_SECRET
tenant_id = var.ARM_TENANT_ID

}


resource "azurerm_resource_group" "main" {
  name     = "main-dev"
  location = var.location
}

## Network
resource "azurerm_virtual_network" "main" {
  name = "main-dev"
  address_space = var.main_address_space

  location            = azurerm_resource_group.main.location
  resource_group_name = azurerm_resource_group.main.name
}

resource "azurerm_subnet" "subnet_1" {
  name                 = "subnet-1"
  resource_group_name  = azurerm_resource_group.main.name
  virtual_network_name = azurerm_virtual_network.main.name
  address_prefixes     = var.main_address_prefixes

  delegation {
    name = "delegation"



  service_delegation {
  name    = "Microsoft.ContainerInstance/containerGroups"
  actions = ["Microsoft.Network/virtualNetworks/subnets/join/action", "Microsoft.Network/virtualNetworks/subnets/prepareNetworkPolicies/action"]
  }
  }
}

## NSG

resource "azurerm_network_security_group" "mainSubnet1NSG" {
  name                = "subnet-1-nsg"
  location            = azurerm_resource_group.main.location
  resource_group_name = azurerm_resource_group.main.name

  # ALARM!: This security rule allow all incoming tcp traffic without any restrictions!
  # Outbound rules missing.
  security_rule {
    name = "test123"
    priority = 100
    direction = "Inbound"
    access = "Allow"
    protocol = "Tcp"
    source_port_range = "*"
    destination_port_range = "*"
    source_address_prefix = "*"
    destination_address_prefix = "*"
  }
}

## AKS
resource "azurerm_kubernetes_cluster" "k8s" {
  name = "example-aks1"
  location = azurerm_resource_group.main.location
  resource_group_name = azurerm_resource_group.main.name
  dns_prefix = "exampleaks1"

  # Default requirements not justified. What kind of resources is the app expected to use? Worst case, best case? Automated test backing up this decision.
  default_node_pool {
    name       = "default"
    node_count = 1
    vm_size    = "Standard_D2_v2"
  }

  identity {
    type = "SystemAssigned"
  }
}

## KeyVault
data "azurerm_client_config" "current" {}

resource "random_id" "name" {
  byte_length = 8
}

resource "azurerm_key_vault" "vault" {
  name                       = "vault${random_id.name.hex}"
  location                   = azurerm_resource_group.main.location
  resource_group_name        = azurerm_resource_group.main.name
  tenant_id                  = data.azurerm_client_config.current.tenant_id
  
  # Can we use the free version here? Where are you using this premium license from?
  sku_name                   = "premium"

  # 
  soft_delete_retention_days = 7

  access_policy {
    tenant_id = data.azurerm_client_config.current.tenant_id
    object_id = data.azurerm_client_config.current.object_id

    key_permissions = [
      "Create",
      "Get",
    ]

    secret_permissions = [
      "Set",
      "Get",
      "Delete",
      "Purge",
      "Recover"
    ]
  }
}

resource "azurerm_key_vault_access_policy" "csi_access_policy" {
  key_vault_id = azurerm_key_vault.vault.id
  tenant_id    = data.azurerm_client_config.current.tenant_id
  object_id    = azurerm_kubernetes_cluster.k8s.kubelet_identity.0.object_id

  certificate_permissions = [
    "Get"
  ]

  key_permissions = [
    "Get"
  ]

  secret_permissions = [
    "Get"
  ]
}

resource "azurerm_key_vault_secret" "license" {
  name         = "license"
  value        = var.licenseKey
  key_vault_id = azurerm_key_vault.vault.id
}


/*
This Terraform code creates an Azure infrastructure including a resource group, virtual network, subnet, network security group, AKS cluster, and Key Vault.

  positive:
      Variables are declared for reusable and flexible infrastructure deployment
    Use of provider's features for Key Vault to manage soft-deleted secrets during resource destruction
    Use of SystemAssigned Identity for AKS
    Use of premium SKU for Key Vault for added security and reliability



    Security:

    The code contains sensitive information such as the client secret for the service principal, which is appropriately marked as "sensitive".
    The code creates a default security rule that allows all inbound TCP traffic, which is not secure.

    Network settings:

    The virtual network and subnet are created, and a delegation is set for the subnet to join and prepare network policies for the Container Instance.

    Performance:

    The AKS cluster is set with a default node pool of 1 node and size "Standard_D2_v2". This can be adjusted based on the required performance needs.
    The Key Vault is set with a premium SKU and soft deletion retention of 7 days.

    Other:

    The Key Vault access policy is set for the tenant and object ID from the client config, with specific permissions for keys and secrets.
    The location for the resources is not defined, so Terraform will prompt for it.
    The "main_address_space" and "main_address_prefixes" are not defined, so Terraform will prompt for them.

  
    Consider using Terraform outputs to print or store important resource information
    Add input validation and/or constraints to the input variables to prevent invalid values from being used:

    variable "example_var" {
      type = string
    }

    locals {
      validation_result = type(var.example_var) == string
      lenght_validation = length(var.example_var) >= 5

    }

    output "validation_result" {
      value = local.validation_result
    }

This code can be improved further by defining the location, main_address_space, and main_address_prefixes, and adjusting the security rule to be more secure.


## aditionally creating a module for this would be a good idea. The code for both env is repeated and can be easily change to be configurable for both envs.
*/